package testes;

import dao.DAOGenerico;
import entidade.Avaliador;
import entidade.Cidade;
import entidade.Estado;
import entidade.Formacao;
import java.util.Date;

public class TesteAvaliador {

    public static void main(String args[]) {

        DAOGenerico dao = new DAOGenerico();
        Cidade c = new Cidade();
        Formacao f = new Formacao();
        Estado e = new Estado();
        e.setNome("Salvador");
        e.setSigla("SA");

        f.setFormacao("ensino superior");
        c.setNome("cidade");
        c.setEstado(e);

        Avaliador a = new Avaliador();

        a.setNome("Joao");
        a.setCpf("049.039.679-80");
        a.setTelefone("(44) 3432-1133");
        a.setEmail("joao@gmail.com");
        a.setEndereco("Rua sat catarina 33");
        //  p.setCidade(c);
        a.setEspecializacao("teste");
        //  p.setFormacao(f);
        a.setLogin("juliano");
        a.setPermissao("ROLE_ADMIN");
        a.setSenha("sw8rdoi5!");
        a.setDataDeCadastro(new Date());
        dao.inserir(a);

    }

}
