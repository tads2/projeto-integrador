package testes;

import dao.DAOGenerico;
import entidade.Administrador;
import java.util.Date;

public class TesteAdministrador {

    public static void main(String args[]) {

        Administrador a = new Administrador();
        DAOGenerico dao = new DAOGenerico();

        a.setNome("Administrador");
        a.setCpf("049.039.679-79");
        a.setTelefone("(44) 3432-1133");
        a.setEmail("joao@gmail.com");
        a.setEndereco("Rua sat catarina 33");

        a.setLogin("admin");
        a.setPermissao("ROLE_ADMIN");
        a.setSenha("123");
        a.setDataDeCadastro(new Date());
        dao.inserir(a);
    }
}
