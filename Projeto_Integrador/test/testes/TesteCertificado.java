
package testes;

import controle.ControleEvento;
import dao.DAOGenerico;
import entidade.Administrador;
import entidade.CadastroEvento;
import entidade.Certificado;
import entidade.Evento;
import entidade.Participante;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TesteCertificado {
    
    public static void main(String args[]) {

        Participante a = new Participante();
        Evento evento = new Evento();
        CadastroEvento c = new CadastroEvento();
        Certificado ce = new Certificado();
        DAOGenerico dao = new DAOGenerico();
        
        List<CadastroEvento> contr = new ArrayList<CadastroEvento>();
        
       contr = dao.listar(CadastroEvento.class);
       
        for (CadastroEvento cadastroEvento : contr) {
            System.out.println(cadastroEvento.getEvento().getNome());
            System.out.println(cadastroEvento.getParticipante().getNome());
            ce.setEvento(cadastroEvento.getEvento());

            dao.inserir(ce);
            
        }
    }
    
    
}
