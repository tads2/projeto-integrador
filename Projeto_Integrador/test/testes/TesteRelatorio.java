/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;


import controle.ControleRelatorio;
import dao.DAOGenerico;
import entidade.CadastroEvento;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class TesteRelatorio {
    
       public static void main(String args[]) {

        DAOGenerico dao = new DAOGenerico();
        ControleRelatorio controleRelatorio = new ControleRelatorio();
        
        List<CadastroEvento> l=dao.listar(CadastroEvento.class);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        for (CadastroEvento cadastroEvento : l) {
          

            System.out.println(cadastroEvento.getEvento().getPalestrante().getNome());
            System.out.println(cadastroEvento.getParticipante().getNome());
            
        }
       }
}
