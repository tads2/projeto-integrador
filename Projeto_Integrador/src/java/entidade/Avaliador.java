
package entidade;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;



@Entity
@DiscriminatorValue (value = "avaliador")
public class Avaliador extends Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
   

    @Column(name="especializacao", length = 150)
    private String especializacao;
  

    public void setEspecializacao(String especializacao) {
        this.especializacao = especializacao;
    }

    public String getEspecializacao() {
        return especializacao;
    }

   
    
}
