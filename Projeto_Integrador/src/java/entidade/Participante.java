package entidade;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
@DiscriminatorValue (value = "participante")
public class Participante extends Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "matricula", length = 20)
    private String matricula;

  

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
  }
