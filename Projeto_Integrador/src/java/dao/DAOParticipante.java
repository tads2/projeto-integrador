package dao;

import entidade.Participante;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class DAOParticipante {

    private EntityManager em;

    public void salvar(Participante participante) {

        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(participante);
        em.getTransaction().commit();
    }

    public void alterar(Participante participante) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(participante);
        em.getTransaction().commit();
    }

    public void remover(Participante participante) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.remove(participante);
        em.getTransaction().commit();
    }

    public void buscaId2() {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        Participante participante = em.find(Participante.class, 2L);
        System.out.println("estado: " + participante.getNome());
        em.getTransaction().commit();
    }

    public List<Participante> listaParticipante() {
        List<Participante> listaParticipante;
        em = CriarEntityManager.getInstancia().getEm();
        listaParticipante = em.createQuery("from participante_Z").getResultList();
        return listaParticipante;
    }




}
