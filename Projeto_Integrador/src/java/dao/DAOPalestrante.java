
package dao;

import dao.interfaces.DAOInterface;
import entidade.Palestrante;
import java.util.List;
import javax.persistence.EntityManager;


public class DAOPalestrante implements DAOInterface{
    
     private EntityManager em;

     @Override
    public void salvar(Object palestrante) {

        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(palestrante);
        em.getTransaction().commit();
    }

     @Override
    public void alterar(Object palestrante) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(palestrante);
        em.getTransaction().commit();
    }

     @Override
    public void remover(Object palestrante) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.remove(palestrante);
        em.getTransaction().commit();
    }

     @Override
    public void buscaId2() {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        Palestrante palestrante = em.find(Palestrante.class, 2L);
        System.out.println("palestrante: " + palestrante.getNome());
        em.getTransaction().commit();
    }

     @Override
    public List<Palestrante> listaPalestrante() {
        List<Palestrante> listaPalestrante;
        em = CriarEntityManager.getInstancia().getEm();
        listaPalestrante = em.createQuery("from palestrante").getResultList();
        return listaPalestrante;
    }
    
    
}
