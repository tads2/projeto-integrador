
package dao;

import entidade.Estado;
import entidade.Evento;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;


public class DAOEvento {
     private EntityManager em;

    public void salvar(Evento evento) {

        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(evento);
        em.getTransaction().commit();
    }

    public void alterar(Evento evento) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(evento);
        em.getTransaction().commit();
    }

    public void remover(Evento evento) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        em.remove(evento);
        em.getTransaction().commit();
    }

    public void buscaId2() {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        Evento evento = em.find(Evento.class, 2L);
        System.out.println("estado: " + evento.getNome());
        em.getTransaction().commit();
    }

    public List<Evento> listaEvento() {
        List<Evento> listaEvento;
        em = CriarEntityManager.getInstancia().getEm();
        listaEvento = em.createQuery("from evento").getResultList();
        return listaEvento;
    }
    
       public Long count(Long id){  
      em = CriarEntityManager.getInstancia().getEm();
        Query q = em.createQuery("select count(*) from CadastroEvento where evento.id = "+id);  
        return (Long) q.getSingleResult();  
   } 
    
    
    
    
}
