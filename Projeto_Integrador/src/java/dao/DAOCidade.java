
package dao;

import entidade.Cidade;
import java.util.List;
import javax.persistence.EntityManager;


public class DAOCidade {
    
        private EntityManager em;
         DAOGenerico daoGen = new DAOGenerico();

    public void salvar(Cidade cidade) {

        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(cidade);
        em.getTransaction().commit();
    }

    public void alterar(Cidade cidade) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(cidade);
        em.getTransaction().commit();
    }

    public void remover(Cidade cidade) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        em.remove(cidade);
        em.getTransaction().commit();
    }

    public void buscaId2() {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        Cidade cidade = em.find(Cidade.class, 2L);
        System.out.println("cidade: " + cidade.getNome());
        em.getTransaction().commit();
    }

        public List<Cidade> listaCidade() {
        List<Cidade> cidade=daoGen.listar(Cidade.class);
        return cidade;
    }
    
}
