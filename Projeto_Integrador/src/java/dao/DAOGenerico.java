package dao;

import java.util.List;
import javax.persistence.EntityManager;
import dao.interfaces.DAOInterfaceGenerico;

public class DAOGenerico implements DAOInterfaceGenerico{

    private EntityManager em;

    @Override
    public void inserir(Object obj) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
    }

    @Override
    public void alterar(Object obj) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(obj);
        em.getTransaction().commit();
    }

    @Override
    public void remover(Class classe, Long id) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        Object obj = em.find(classe, id);
        if (obj != null) {
            em.remove(obj);
        }
        em.getTransaction().commit();
    }

    @Override
    public Object buscarPorId(Class classe, Long id) {
        em = CriarEntityManager.getInstancia().getEm();
        Object retorno = em.find(classe, id);
        return retorno;
    }

    @Override
    public List buscar(Class classe, String nome) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from Trabalhos where nome like'" + nome + "'").getResultList();
        return retorno;

    }

    @Override
    public List listar(Class classe) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName()).getResultList();
        return retorno;
    }

    @Override
    public List listarCondicao(Class classe, String condicao) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where Login like '" + condicao + "'").getResultList();
        return retorno;
    }

    @Override
    public List listarCondic(Class classe, long id) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where id_evento =" + id).getResultList();
        return retorno;
    }

    @Override
    public List listarCondic2(Class classe, long id) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where evento =" + id).getResultList();
        return retorno;
    }
    
    @Override
        public List listarCondic3(Class classe, long id) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where participante =" + id).getResultList();
        return retorno;
    }

    @Override
    public List listarCondicData(Class classe) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where dataEvento > CURRENT_DATE").getResultList();
        return retorno;
    }
    
    @Override
        public List listarCondicData2(Class classe, Long id) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where participante =" + id +" and evento.dataEvento > CURRENT_DATE").getResultList();
        return retorno;
    }

    @Override
    public List listarCondicaoTrabalhos(Class classe, String login) {
        em = CriarEntityManager.getInstancia().getEm();
        List<Object> retorno = em.createQuery("from " + classe.getSimpleName() + " where participante.login like '" + login + "'").getResultList();
        return retorno;
    }

    @Override
    public Object listarCondicString(Class classe, long id) {
        em = CriarEntityManager.getInstancia().getEm();
        Object retorno = em.createQuery("from " + classe.getSimpleName() + " where id_evento =" + id).getSingleResult();
        return retorno;
    }

}
