
package dao;

import entidade.Estado;
import java.util.List;
import javax.persistence.EntityManager;


public class DAOEstado {
    
            private EntityManager em;
            
            DAOGenerico daoGen = new DAOGenerico();

    public void salvar(Object estado) {

        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(estado);
        em.getTransaction().commit();
    }

    public void alterar(Estado estado) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(estado);
        em.getTransaction().commit();
    }

    public void remover(Estado estado) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        em.remove(estado);
        em.getTransaction().commit();
    }

    public void buscaId2(int id) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        Estado estado = em.find(Estado.class, id);
        System.out.println("estado: " + estado.getNome());
        em.getTransaction().commit();
    }

    public List<Estado> listaEstado() {
        List<Estado> estados=daoGen.listar(Estado.class);
        return estados;
    }
    
}
