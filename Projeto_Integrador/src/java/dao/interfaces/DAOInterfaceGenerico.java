package dao.interfaces;

import java.util.List;

public interface DAOInterfaceGenerico {

    public void inserir(Object obj);

    public void alterar(Object obj);

    public void remover(Class classe, Long id);

    public Object buscarPorId(Class classe, Long id);

    public List buscar(Class classe, String nome);

    public List listar(Class classe);

    public List listarCondicao(Class classe, String condicao);

    public List listarCondic(Class classe, long id);

    public List listarCondic2(Class classe, long id);

    public List listarCondic3(Class classe, long id);

    public List listarCondicData(Class classe);

    public List listarCondicData2(Class classe, Long id);

    public List listarCondicaoTrabalhos(Class classe, String login);

    public Object listarCondicString(Class classe, long id);
    
  

}
