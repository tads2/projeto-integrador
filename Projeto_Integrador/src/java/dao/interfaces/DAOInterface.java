
package dao.interfaces;

import java.util.List;


public interface DAOInterface<E> {
    
    
    public void salvar(E palestrante) ;
    public void alterar(E palestrante) ;
    public void remover(E palestrante) ;
    public void buscaId2() ;

    public List<E> listaPalestrante() ;
    
    
    
}
