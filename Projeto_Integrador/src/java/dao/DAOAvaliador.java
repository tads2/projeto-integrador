package dao;

import entidade.Avaliador;
import java.util.List;
import javax.persistence.EntityManager;

public class DAOAvaliador {

    private EntityManager em;

    public void salvar(Avaliador avaliador) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.persist(avaliador);
        em.getTransaction().commit();
    }

    public void alterar(Avaliador avaliador) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.merge(avaliador);
        em.getTransaction().commit();
    }

    public void remover(Avaliador avaliador) {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        em.remove(avaliador);
        em.getTransaction().commit();
    }

    public void buscaId2() {
        em = CriarEntityManager.getInstancia().getEm();
        em.getTransaction().begin();
        em.getTransaction().begin();
        Avaliador avaliador = em.find(Avaliador.class, 2L);
        System.out.println("Avaliador: " + avaliador.getNome());
        em.getTransaction().commit();
    }

    public List<Avaliador> listaClientes() {
        List<Avaliador> listaAvaliador;
        em = CriarEntityManager.getInstancia().getEm();
        listaAvaliador = em.createQuery("from avaliador").getResultList();
        return listaAvaliador;
    }

}
