package controle;

import dao.DAOGenerico;
import entidade.Avaliador;
import entidade.Evento;
import entidade.Participante;
import entidade.Trabalhos;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

@ManagedBean
@SessionScoped
public class ControleTrabalho implements Serializable {

    Trabalhos trabalhos = new Trabalhos();
    Evento evento = new Evento();
    private DAOGenerico daoGener = new DAOGenerico();
    private String login;
    SecurityContext context = SecurityContextHolder.getContext();

    public Trabalhos getTrabalhos() {
        return trabalhos;
    }

    public Evento getEvento() {
        return evento;
    }
    public void setEvento(Evento evento) {
        this.evento = evento;
    }
    public void setTrabalhos(Trabalhos trabalhos) {
        this.trabalhos = trabalhos;
    }
    public DAOGenerico getDaoGener() {
        return daoGener;
    }
    public void setDaoGener(DAOGenerico daoGener) {
        this.daoGener = daoGener;
    }

    public ControleTrabalho() {
    }

    public void acaoAlterar(Trabalhos trabalhos) {
        this.trabalhos = trabalhos;

    }

    public void enviaTrabalho(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        FacesContext aFacesContext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) aFacesContext.getExternalContext().getContext();
        String realPath = context.getRealPath("/");
        String controleNome = procuraTrabalhos(file.getFileName());
        String caminho = realPath + "recurso" + File.separator + "trabalhos" + File.separator + controleNome;
        String caminhoAlterado = caminho.replace("\\build", "");

        try {
            FileInputStream in = (FileInputStream) file.getInputstream();
            FileOutputStream out = new FileOutputStream(caminhoAlterado);

            byte[] buffer = new byte[(int) file.getSize()];
            int contador = 0;

            while ((contador = in.read(buffer)) != -1) {
                out.write(buffer, 0, contador);
            }
            in.close();
            out.close();

            trabalhos.setArea(evento.getPalestrante().getEspecializacao());

            List<Participante> participantes = procuraParticipante();

            for (Participante participante1 : participantes) {
                trabalhos.setParticipante(participante1);
            }

            trabalhos.setNome(controleNome);
            trabalhos.setSituacao("Aguardando avaliação");
            trabalhos.setCaminhoUrl(caminhoAlterado);
            trabalhos.setEvento(evento);
            daoGener.inserir(trabalhos);

            FacesMessage msg = new FacesMessage("O Arquivo ", file.getFileName() + " salvo em banco de dados.");
            FacesContext.getCurrentInstance().addMessage("msgUpdate", msg);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public List<Trabalhos> procuraTodosOsTrabalhos() {
        List<Trabalhos> trabalhos = daoGener.listar(Trabalhos.class);
        return trabalhos;
    }

    public List<Trabalhos> procuraTrabalhosUsuario() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Trabalhos> trabalhos = daoGener.listarCondicaoTrabalhos(Trabalhos.class, login);
        return trabalhos;
    }

    public String procuraTrabalhos(String nome) {
        List<Trabalhos> trabalhos = daoGener.listar(Trabalhos.class);
        for (Trabalhos trabalho : trabalhos) {
            if (trabalho.getNome() == null || trabalho.getNome().equals(nome)) {
                nome = (trabalhos.size() + 1) + " - " + nome;
            }
        }
        return nome;
    }

    public String excluirTrabalhos(long id) {
        daoGener.remover(trabalhos.getClass(), id);
        return "listarTrabalhos";
    }

    public String visualizaTrabalhos(String nome) {
        String caminho = "..\\recurso\\trabalhos\\" + nome;
        return caminho;
    }

    public String avaliaTrabalhos(Trabalhos trabalho) {
        this.trabalhos = trabalho;
        daoGener.alterar(trabalho);
        return "avaliarTrabalhos.xhtml";

    }

    public String avalia() {
        trabalhos.setSituacao("Avaliado");
        List<Avaliador> avaliadors = procuraAvaliador();
        for (Avaliador avaliador : avaliadors) {
            trabalhos.setAvaliador(avaliador);
        }
        daoGener.alterar(trabalhos);
        return "listarTrabalhos.xhtml";

    }
    public List<Participante> procuraParticipante() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Participante> p = daoGener.listarCondicao(Participante.class, login);
        return p;
    }

    public String enviarTrabalho(Evento evento) {
        this.evento = evento;

        return "enviaTrabalhos";
    }

        public List<Avaliador> procuraAvaliador() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Avaliador> p = daoGener.listarCondicao(Avaliador.class, login);
        return p;
    }

}
