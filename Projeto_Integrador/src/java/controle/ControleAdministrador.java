package controle;

import dao.DAOGenerico;
import dao.DAOParticipante;
import entidade.Administrador;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

@ManagedBean
@SessionScoped
public class ControleAdministrador {

    private Administrador administrador = new Administrador();
    private DAOGenerico daoGener = new DAOGenerico();
    private DAOParticipante dao = new DAOParticipante();
    private String confereSenha;
    private String login;
    SecurityContext context = SecurityContextHolder.getContext();

    public ControleAdministrador() {
    }

    public List<Administrador> procuraAdministrador() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Administrador> p = daoGener.listarCondicao(Administrador.class, login);
        return p;
    }

    public String trocaLink() {
        FacesContext context = FacesContext.getCurrentInstance().getCurrentInstance();
        ExternalContext external = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) external.getRequest();
        if (request.isUserInRole("ROLE_USER")) {
            return "../Projeto_Integrador/usuario/main.xhtml";
        }else if (request.isUserInRole("ROLE_PALES")) {
            return "../Projeto_Integrador/palestrante/main.xhtml";
        } else if (request.isUserInRole("ROLE_ADMIN")) {
            return "../Projeto_Integrador/cadastros/main.xhtml";
        } else if (request.isUserInRole("ROLE_AVAL")) {
            return "../Projeto_Integrador/avaliador/main.xhtml";
        } else {
            return null;
        }
    }
}
