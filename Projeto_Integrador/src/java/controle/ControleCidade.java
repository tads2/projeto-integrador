
package controle;

import dao.DAOCidade;
import dao.DAOEstado;
import dao.DAOGenerico;
import entidade.Cidade;
import entidade.Estado;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class ControleCidade {

    public ControleCidade() {
    }
    private DAOCidade dao = new DAOCidade();
    Cidade cidade = new Cidade();
    public Cidade getCidade() {
        return cidade;
    }

    public List<SelectItem> getEstado() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOEstado daoEstado = new DAOEstado();
        for (Estado e : daoEstado.listaEstado()) {
            item.add(new SelectItem(e, e.getNome()));
        }
        return item;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Cidade> getLista() {
           return dao.listaCidade();
    }

    public String salvar() {
        DAOCidade dao = new DAOCidade();
        dao.salvar(cidade);
        return "index";
    }

    public String acaoAlterar(Cidade cidade) {
        this.cidade = cidade;
        return "cadastroEstado";
    }

    public String altera() {
        DAOCidade dao = new DAOCidade();
        dao.alterar(this.cidade);
        return "cadastroEstado";
    }

     public void excluir(long id) {
        DAOGenerico dao = new DAOGenerico();
        dao.remover(cidade.getClass(), id);
    }
    
        public String limpCidade() {
        cidade = new Cidade();
        return editCidade();
    }

    public String editCidade() {
        return "/cadastros/cadastrarcidade";
    }
    
}
