package controle;

import dao.DAOGenerico;
import entidade.Avaliador;
import entidade.CadastroEvento;
import entidade.Evento;
import entidade.Palestrante;
import entidade.Participante;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import relatorio.Relatorio;
import relatorio.RelatorioBanco;

@ManagedBean
@ViewScoped
public class ControleRelatorio {

    private CadastroEvento cadastroEvento = new CadastroEvento();
    private Evento evento = new Evento();
    private Participante participante = new Participante();
    private DAOGenerico dao = new DAOGenerico();
    private Relatorio relatorio = new Relatorio();

    public ControleRelatorio() {
    }

    public CadastroEvento getCadastroEvento() {
        return cadastroEvento;
    }

    public void setCadastroEvento(CadastroEvento cadastroEvento) {
        this.cadastroEvento = cadastroEvento;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public void gerarRelatorioParticipante() {
        String caminho = "/relatorio/relatorioParticipante.jasper";

        List<Object> participantes = dao.listar(Participante.class);
        relatorio.getRelatorio(participantes, caminho);
    }

    public void gerarRelatorioPalestrante() {
        String caminho = "/relatorio/relatorioPalestrante.jasper";
        relatorio.getRelatorio(dao.listar(Palestrante.class), caminho);
    }

    public void gerarRelatorioAvaliador() {
        String caminho = "/relatorio/relatorioAvaliador.jasper";
        relatorio.getRelatorio(dao.listar(Avaliador.class), caminho);
    }

    public void gerarLista(Long id) {
        String caminho = "/relatorio/listaPresenca.jasper";
        List<Object> lista = dao.listarCondic(CadastroEvento.class, id);
        relatorio.getRelatorio(lista, caminho);
    }

    public void gerarListaPresenca(Evento evento) {
        this.evento = evento;
        String caminho = "/relatorio/listaPresenca.jasper";
        relatorio.getRelatorio(dao.listarCondic(CadastroEvento.class, evento.getId()), caminho);
    }
    public void emitir() {
        RelatorioBanco banco = new RelatorioBanco();
        banco.imprimeRelatorio("relatorioParticipante.jasper", null, "Rel_Participante");
    }

}
