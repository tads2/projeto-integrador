package controle;

import dao.DAOEvento;
import dao.DAOGenerico;
import entidade.CadastroEvento;
import entidade.Certificado;
import entidade.Evento;
import entidade.Palestrante;
import entidade.Participante;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import relatorio.Relatorio;

@ManagedBean
@SessionScoped
public class ControleCadastroEvento {

    private Evento evento = new Evento();

    private Participante participante = new Participante();
    private Certificado certificado = new Certificado();
    private Palestrante palestrante = new Palestrante();
    private DAOGenerico dao = new DAOGenerico();
    private DAOEvento daoe = new DAOEvento();
    private CadastroEvento cadastroEvento = new CadastroEvento();
    private String confereSenha;
    private String login;
    SecurityContext context = SecurityContextHolder.getContext();
    private Long numeroVagas;
    private boolean ativos;

    public ControleCadastroEvento() {

    }

    public boolean isAtivos() {
        return ativos;
    }

    public void setAtivos(boolean ativos) {
        this.ativos = ativos;
    }

    public Long getNumeroVagas(Evento evento) {

        return evento.getNumeroVagas() - (numeroVagas = daoe.count(evento.getId()));
    }

    public void setNumeroVagas(Long numeroVagas) {
        this.numeroVagas = numeroVagas;
    }

    public List<SelectItem> getPalestrante() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        List<Palestrante> palestrante = dao.listar(Palestrante.class);
        for (Palestrante p : palestrante) {
            item.add(new SelectItem(p, p.getNome()));
        }
        return item;
    }

    public List<Evento> getLista() {
        return listaEvento();
    }

    public Certificado getCertificado() {
        return certificado;
    }

    public void setCertificado(Certificado certificado) {
        this.certificado = certificado;
    }

    public void salvar(Evento evento) {

        this.evento = evento;
        cadastroEvento.setEvento(evento);
        if (dao.listar(CadastroEvento.class).size() <= evento.getNumeroVagas()) {
            List<Participante> participantes = procuraParticipante();

            if (dao.buscarPorId(CadastroEvento.class, evento.getId()) == null) {
                for (Participante participante1 : participantes) {
                    cadastroEvento.setParticipante(participante1);
                }

                dao.inserir(cadastroEvento);
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Sua inscrição no evento " + evento.getNome().toLowerCase() + " foi efetuada com sucesso", ""));
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Você já está inscrito no evento " + evento.getNome().toLowerCase(), ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "O evento " + evento.getNome().toLowerCase() + " já esta com o numero de vagas completo", ""));

        }

    }

    public String acaoAlterar(CadastroEvento cadastroEvento) {
        this.cadastroEvento = cadastroEvento;
        return "alterarEvento";
    }

    public void alterar() {
        dao.alterar(cadastroEvento);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));

    }

    public void excluirEvento(long id) {
        dao.remover(cadastroEvento.getClass(), id);

    }

    public CadastroEvento getCadastroEvento() {
        return cadastroEvento;
    }

    public void setCadastroEvento(CadastroEvento cadastroEvento) {
        this.cadastroEvento = cadastroEvento;
    }

    public List<Evento> listaEvento() {
        List<Evento> eventos = dao.listar(Evento.class);
        return eventos;
    }

    public String limpaEvento() {
        cadastroEvento = new CadastroEvento();
        return editCadastroEvento();
    }

    public String editCadastroEvento() {
        this.cadastroEvento = cadastroEvento;
        return "cadastrarEvento.xhtml";
    }

    public List<Participante> procuraParticipante() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }

        List<Participante> p = dao.listarCondicao(Participante.class, login);
        return p;
    }

    public void validarFalta(CadastroEvento cadastroEvento) {
        this.cadastroEvento = cadastroEvento;
        certificado.setEvento(cadastroEvento.getEvento());
        certificado.setValidacao("FALTA");
        dao.inserir(certificado);
        cadastroEvento.setCertificado(certificado);
        dao.alterar(cadastroEvento);

        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Validação efetuada com sucesso", ""));

    }

    public void validarPresenca(CadastroEvento cadastroEvento) {
        this.cadastroEvento = cadastroEvento;
        certificado.setEvento(cadastroEvento.getEvento());
        certificado.setValidacao("PARTICIPOU");
        dao.inserir(certificado);
        cadastroEvento.setCertificado(certificado);
        dao.alterar(cadastroEvento);

        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Validação efetuada com sucesso", ""));

    }

    public List<CadastroEvento> listaCadastroEventoInscrito() {
        List<Participante> participantes = procuraParticipante();
        Long controle = null;
        for (Participante participante1 : participantes) {
            controle = participante1.getId();
        }
        if (ativos == false) {
            List<CadastroEvento> cadastro = dao.listarCondic3(CadastroEvento.class, controle);
            return cadastro;
        } else {
            List<CadastroEvento> cadastros = dao.listarCondicData2(CadastroEvento.class, participante.getId());
            return cadastros;
        }
    }

    public void gerarCertificadoEvento(Evento e) {
        String caminho = "/relatorio/certifc.jasper";
        Relatorio relatorio = new Relatorio();
        List<Object> lista = dao.listarCondic(CadastroEvento.class, e.getId());
        relatorio.getRelatorio(lista, caminho);
    }
}
