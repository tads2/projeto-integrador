package controle;

import dao.DAOCidade;
import dao.DAOFormacao;
import dao.DAOGenerico;
import dao.DAOPalestrante;
import entidade.Cidade;
import entidade.Formacao;
import entidade.Palestrante;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

@ManagedBean
@SessionScoped
public class ControlePalestrante {

    private Palestrante palestrante = new Palestrante();
    private DAOGenerico daoGener = new DAOGenerico();
    private DAOPalestrante dao = new DAOPalestrante();
    private String confereSenha;
    private String login;
    SecurityContext context = SecurityContextHolder.getContext();

    public ControlePalestrante() {
    }

    public List<SelectItem> getFormacao() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOFormacao daoFor = new DAOFormacao();
        for (Formacao f : daoFor.listaFormacao()) {
            item.add(new SelectItem(f, f.getFormacao()));
        }
        return item;
    }

    public List<SelectItem> getCidade() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOCidade daoCid = new DAOCidade();
        for (Cidade c : daoCid.listaCidade()) {
            item.add(new SelectItem(c, c.getNome()));
        }
        return item;
    }

    public List<Palestrante> getLista() {
        return listaPar();
    }

    public String acaoAlterar(Palestrante palestrante) {
        this.palestrante = palestrante;
        return "alterarPalestrante";
    }

    public String excluirPalestrante(long id) {
        daoGener.remover(palestrante.getClass(), id);
        return "consultarPalestrante";
    }

    public Palestrante getPalestrante() {
        return palestrante;
    }

    public void setParticipante(Palestrante palestrante) {
        this.palestrante = palestrante;
    }

    public List<Palestrante> listaPar() {
        List<Palestrante> palestrante = daoGener.listar(Palestrante.class);
        return palestrante;
    }

    private void insertPessoa() {

        if (palestrante.getSenha() == null ? confereSenha == null : palestrante.getSenha().equals(confereSenha)) {
            palestrante.setPermissao("ROLE_PALES");
            daoGener.inserir(palestrante);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Gravação efetuada com sucesso", ""));

        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "As senhas não conferem.", ""));
        }
    }

    public String limpPessoa() {
        palestrante = new Palestrante();
        return editPessoa();
    }

    public String editPessoa() {
        return "cadastrarPalestrante.xhtml";
    }

    public String addPessoa() {
        Date date = new Date();
        if (palestrante.getId() == null || palestrante.getId() == 0) {
            palestrante.setDataDeCadastro(date);
            insertPessoa();
        } else {
            updatePessoa();
        }

        return null;
    }

    private void updatePessoa() {
        daoGener.alterar(palestrante);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));
    }

    public String getConfereSenha() {
        return confereSenha;
    }

    public void setConfereSenha(String confereSenha) {
        this.confereSenha = confereSenha;
    }

    public List<Palestrante> procuraPalestrante() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Palestrante> p = daoGener.listarCondicao(Palestrante.class, login);
        return p;
    }
}
