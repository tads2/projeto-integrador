package controle;

import dao.DAOCidade;
import dao.DAOFormacao;
import dao.DAOGenerico;
import dao.DAOParticipante;
import entidade.Cidade;
import entidade.Formacao;
import entidade.Participante;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

@ManagedBean
@SessionScoped
public class ControleParticipante {

    private Participante participante = new Participante();
    private DAOGenerico daoGener = new DAOGenerico();
    private DAOParticipante dao = new DAOParticipante();
    private String confereSenha;
    private String login;
    SecurityContext context = SecurityContextHolder.getContext();

    public ControleParticipante() {
    }

    public List<SelectItem> getFormacao() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOFormacao daoFor = new DAOFormacao();
        for (Formacao f : daoFor.listaFormacao()) {
            item.add(new SelectItem(f, f.getFormacao()));
        }
        return item;
    }

    public List<SelectItem> getCidade() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOCidade daoCid = new DAOCidade();
        for (Cidade c : daoCid.listaCidade()) {
            item.add(new SelectItem(c, c.getNome()));
        }
        return item;
    }

    public List<Participante> getLista() {

        return listaPar();
    }

    public String paginaCondicao() {
        return "cadastrarParticipante";
    }

    public String acaoAlterar(Participante participante) {
        this.participante = participante;
        return "alterarParticipante";
    }

    public String excluirParticipante(long id) {
        daoGener.remover(participante.getClass(), id);
        return "consultarParticipante";
    }

    public Participante getParticipante() {
        return participante;
    }

    public void setParticipante(Participante participante) {
        this.participante = participante;
    }

    public List<Participante> listaPar() {
        List<Participante> participante = daoGener.listar(Participante.class);
        return participante;
    }

    private void insertPessoa() {
        if (participante.getSenha() == null ? confereSenha == null : participante.getSenha().equals(confereSenha)) {
            participante.setPermissao("ROLE_USER");
            daoGener.inserir(participante);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Gravação efetuada com sucesso", ""));
            limpPessoa();

        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "As senhas não conferem.", ""));
        }
    }

    public String limpPessoa() {
        participante = new Participante();
        return editPessoa();
    }

    public String editPessoa() {
        this.participante = participante;
        return "alterarParticipante.xhtml";
    }

    public String addPessoa() {
        Date date = new Date();
        if (participante.getId() == null || participante.getId() == 0) {
            participante.setDataDeCadastro(date);
            insertPessoa();
        } else {
            updatePessoa();
        }
        return null;
    }

    private void updatePessoa() {
        daoGener.alterar(participante);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));
    }

    public String getConfereSenha() {
        return confereSenha;
    }

    public void setConfereSenha(String confereSenha) {
        this.confereSenha = confereSenha;
    }

    public List<Participante> procuraParticipante() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Participante> p = daoGener.listarCondicao(Participante.class, login);
        return p;
    }

}
