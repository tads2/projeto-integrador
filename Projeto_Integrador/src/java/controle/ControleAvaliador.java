package controle;

import dao.DAOAvaliador;
import dao.DAOCidade;
import dao.DAOFormacao;
import dao.DAOGenerico;
import entidade.Avaliador;
import entidade.Cidade;
import entidade.Formacao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

@ManagedBean
@SessionScoped
public class ControleAvaliador {

    public ControleAvaliador() {
    }

    private Avaliador avaliador = new Avaliador();
    private DAOGenerico daoGener = new DAOGenerico();
    private DAOAvaliador dao = new DAOAvaliador();
    private String confereSenha;
    private String login;
    SecurityContext context = SecurityContextHolder.getContext();

    public List<SelectItem> getFormacao() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOFormacao daoFor = new DAOFormacao();
        for (Formacao f : daoFor.listaFormacao()) {
            item.add(new SelectItem(f, f.getFormacao()));
        }
        return item;
    }

    public List<SelectItem> getCidade() {
        List<SelectItem> item = new ArrayList<SelectItem>();
        DAOCidade daoCid = new DAOCidade();
        for (Cidade c : daoCid.listaCidade()) {
            item.add(new SelectItem(c, c.getNome()));
        }
        return item;
    }

    public List<Avaliador> getLista() {
        return listaAva();
    }

    public String acaoAlterar(Avaliador avaliador) {
        this.avaliador = avaliador;

        return "alterarAvaliador";
    }

    public String excluirAvaliador(long id) {
        daoGener.remover(avaliador.getClass(), id);
        return "consultarAvaliador";
    }

    public Avaliador getAvaliador() {
        return avaliador;
    }

    public void setAvaliador(Avaliador avaliadorZ) {
        this.avaliador = avaliador;
    }

    public List<Avaliador> listaAva() {
        List<Avaliador> avaliador = daoGener.listar(Avaliador.class);
        return avaliador;
    }

    private void insertPessoa() {
        if (avaliador.getSenha() == null ? confereSenha == null : avaliador.getSenha().equals(confereSenha)) {
            avaliador.setPermissao("ROLE_AVAL");
            daoGener.inserir(avaliador);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Gravação efetuada com sucesso", ""));
            limpPessoa();
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "As senhas não conferem.", ""));
        }
    }

    public String limpPessoa() {
        avaliador = new Avaliador();
        return editPessoa();
    }

    public String editPessoa() {
        return "./../alterarParticipante.xhtml";
    }

    public String addPessoa() {
        Date date = new Date();
        if (avaliador.getId() == null || avaliador.getId() == 0) {
            avaliador.setDataDeCadastro(date);
            insertPessoa();
        } else {
            updatePessoa();
        }
        return null;
    }

    private void updatePessoa() {
        daoGener.alterar(avaliador);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));
        limpPessoa();

    }

    public String getConfereSenha() {
        return confereSenha;
    }

    public void setConfereSenha(String confereSenha) {
        this.confereSenha = confereSenha;
    }

    public List<Avaliador> procuraAvaliador() {
        if (context instanceof SecurityContext) {
            Authentication authentication = context.getAuthentication();
            if (authentication instanceof Authentication) {
                login = (((User) authentication.getPrincipal()).getUsername());
            }
        }
        List<Avaliador> p = daoGener.listarCondicao(Avaliador.class, login);
        return p;
    }

}
