package controle;

import dao.DAOEstado;
import dao.DAOGenerico;
import entidade.Estado;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ControleEstado {

    private DAOEstado dao = new DAOEstado();
    Estado estado = new Estado();

    public ControleEstado() {

    }

    public String limpEstado() {
        estado = new Estado();
        return editEstado();
    }

    public String editEstado() {
        return "/cadastros/cadastrarcidade";
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Estado> getLista() {
        return dao.listaEstado();
    }

    public String salvar() {
        DAOGenerico dao = new DAOGenerico();
        dao.inserir(estado);
        return "cadastrarcidade";
    }

    public String acaoAlterar(Estado estado) {

        this.estado = estado;

        return "cadastroEstado";
    }

    public String alterar(Estado estado) {
        DAOEstado dao = new DAOEstado();
        dao.alterar(estado);
        return "cadastroEstado";
    }

    public void excluir(long id) {
        DAOGenerico dao = new DAOGenerico();
        dao.remover(estado.getClass(), id);
       
    }

}
