package controle;

import dao.DAOGenerico;
import entidade.Agenda;
import entidade.CadastroEvento;
import entidade.Evento;
import entidade.Palestrante;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean
@SessionScoped
public class ControleEvento {

    private Palestrante palestrante = new Palestrante();
    private Evento evento = new Evento();
    private Agenda agenda = new Agenda();
    private DAOGenerico dao = new DAOGenerico();
    private boolean ativos;

    public ControleEvento() {
    }

    public List<SelectItem> getPalestrante() {

        List<SelectItem> item = new ArrayList<SelectItem>();
        List<Palestrante> palestrante = dao.listar(Palestrante.class);
        for (Palestrante p : palestrante) {
            item.add(new SelectItem(p, p.getNome()));
        }
        return item;
    }

    public boolean isAtivos() {
        return ativos;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public void setAtivos(boolean ativos) {
        this.ativos = ativos;
    }

    public List<Agenda> getLista() {
        List<Agenda> agendas = dao.listarCondicData(Agenda.class);
        return agendas;
    }

    public void salvar() {
        if (evento.getId() == null || evento.getId() == 0) {
            dao.inserir(agenda);
            agenda.setEvento(evento);
            dao.inserir(evento);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Gravação efetuada com sucesso", ""));
        } else {
            alterar();

        }
    }

    public String acaoAlterar(Evento evento, Agenda agenda) {
        this.evento = evento;
        this.agenda = agenda;
        return "cadastraEvento";
    }

    public void alterar() {
        dao.alterar(evento);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualização efetuada com sucesso", ""));
    }

    public void excluirEvento(long id) {

        dao.remover(CadastroEvento.class, id);
        dao.remover(agenda.getClass(), id);
        dao.remover(evento.getClass(), id);
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Evento excluido com sucesso", ""));
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public List<Agenda> listaAgenda() {
        if (ativos == false) {
            List<Agenda> agenda = dao.listar(Agenda.class);
            return agenda;
        } else {
            List<Agenda> agendas = dao.listarCondicData(Agenda.class);
            return agendas;
        }
    }

    public String limpaEvento() {
        evento = new Evento();
        return "cadastrarEvento.xhtml";
    }

    public String editEvento(Evento evento) {
        this.evento = evento;
        return "cadastrarEvento.xhtml";
    }

    public String dataEvento() {
        return "dataEvento.xhtml";
    }

    public String validaCerticidados(Evento evento) {
        this.evento = evento;
        return "listaCadastroEvento.xhtml";
    }

    public List listaImagem() {
        List<String> lista = new ArrayList<String>();
        List<Agenda> agendas = dao.listarCondicData(Agenda.class);
        if (agendas.size() != 0) {
            for (Agenda agenda1 : agendas) {
                lista.add("/recurso/imagen/" + agenda1.getEvento().getCartaz());
            }
        } else {
            lista.add("/recurso/imagen/index.jpg");
        }
        return lista;
    }

    public String eventoS(Evento evento) {
        this.evento = evento;
        return "enviaCartaz.xhtml";
    }

    public void enviaCartaz(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        FacesContext aFacesContext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) aFacesContext.getExternalContext().getContext();
        String realPath = context.getRealPath("/");
        String controleNome = procuraCartaz(file.getFileName());
        String caminho = realPath + "recurso" + File.separator + "imagen" + File.separator + controleNome;
        String caminhoAlterado = caminho.replace("\\build", "");

        try {
            FileInputStream in = (FileInputStream) file.getInputstream();
            FileOutputStream out = new FileOutputStream(caminhoAlterado);
            byte[] buffer = new byte[(int) file.getSize()];
            int contador = 0;
            while ((contador = in.read(buffer)) != -1) {
                out.write(buffer, 0, contador);
            }
            in.close();
            out.close();
            evento.setCartaz(controleNome);
            dao.alterar(evento);

            FacesMessage msg = new FacesMessage("O cartaz ", file.getFileName() + " do evento " + evento.getNome() + " foi enviado com sucesso.");
            FacesContext.getCurrentInstance().addMessage("msgUpdate", msg);

        } catch (IOException ioe) {
            ioe.printStackTrace();

        }
    }

    public String procuraCartaz(String nome) {
        List<Evento> eventos = dao.listar(Evento.class);
        for (Evento evento : eventos) {
            if (evento.getCartaz() == null || evento.getCartaz().equals(nome)) {
                nome = (eventos.size() + 1) + " - " + nome;
            }
        }
        return nome;
    }

    public List<CadastroEvento> listaCadastroEvento() {
        List<CadastroEvento> cadastroEventos = dao.listarCondic(CadastroEvento.class, evento.getId());
        return cadastroEventos;
    }

    public void detalheEvento(Evento evento) {
        this.evento = evento;
        if (evento.getId() != null) {
            evento.getNome();
            evento.getLocal();
            evento.getTipoEvento();
        }

    }

    public List listaEventosAtivos() {
        List<Agenda> agendas = dao.listarCondicData(agenda.getClass());
        return agendas;
    }

}
